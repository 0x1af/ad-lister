from loadconfig import Config
# TODO: switch to https://pypi.org/project/HiYaPyCo/

import os
import argparse
import yaml
from jinja2 import Template, Undefined
from collections import OrderedDict
import pdb
import glob
import copy
import traceback
import sys


def resolve_template_inheritance(templates):
	resolved_inheritance = []
	tmp_unresolved = [key for key in templates.keys()]
	
	while len(tmp_unresolved) > 0:
		len_before = len(tmp_unresolved)
		for name in tmp_unresolved[:]: #  [:], as a copy, because elements are removed from the original
			content = templates[name]
			if not "inherit" in content:
				resolved_inheritance.insert(0, name)
				tmp_unresolved.remove(name)
				continue
			try:
				last_parent_pos = max([resolved_inheritance.index(parent) for parent in content["inherit"]]) # if all parents are already resolved, the child can be added right behind its parent
				resolved_inheritance.insert(last_parent_pos+1, name)
				tmp_unresolved.remove(name)
			except ValueError:
				pass # not yet resolved, will be last for now
		if len_before == len(tmp_unresolved):
			raise RuntimeError("Unresolved inheritance. unresolved: %s, resolved: %s, "%(str(tmp_unresolved), str(resolved_inheritance)))

	return resolved_inheritance

def resolve_appends(template):
	template_dicts = [template]

	for dic in template_dicts:
		delete_keys = [] # to avoid: RuntimeError: dictionary changed size during iteration
		add_items_dic = {} # to avoid: RuntimeError: dictionary changed size during iteration
		for key, item in dic.items():
			if key.endswith("_append"):
				target_key = key.rsplit("_append", 1)[0]
				if target_key not in dic:
					add_items_dic[target_key] = item
				elif type(item) != type(dic[target_key]):
					raise Exception("Found key '%s', but cannot append to '%s', as it is not of type '%s'"%(key, target_key, str(type(dic[target_key]))))
				elif type(item) is dict:
					dic[target_key].update(item)
				elif type(item) is list or str:
					dic[target_key] += item
				else:
					raise Exception("Found key '%s', but type '%s' is unsupported for '*_append'"%(key, str(type(item))) )
				delete_keys.append(key)
			elif type(item) is dict:
				template_dicts.append(item)

		for key in delete_keys:
			dic.pop(key)
		for key, item in add_items_dic.items():
			dic[key] = item

def merge_inherited_templates(child, templates):
	result = {}
	
	for parent in child.get("inherit", []):
		print("parent: %s"%(parent))
		result.update(copy.deepcopy(templates[parent])) # A deep copy is required as otherwise non primitive items (dicts, lists) would be shared and overwritten by each template
		resolve_appends(result)
	result.update(copy.deepcopy(child))

	return result

def load_templates(templates_file):
	templates = {}
	with open(templates_file) as fh: 
		templates = yaml.load(fh)
	
	resolved_inheritance = resolve_template_inheritance(templates)
	
	inherited_templates = {}

	for name in resolved_inheritance:
		inherited_templates[name] = merge_inherited_templates(templates[name], inherited_templates)
		
	return inherited_templates

def load_ad(ad_file, templates):
	ad = {}
	prev_path = os.getcwd()
	ad_file_path = os.path.dirname(os.path.realpath(ad_file))
	with open(ad_file) as fh:
		ad = yaml.load(fh)
	
	ad = merge_inherited_templates(ad, templates)
	resolve_appends(ad)
	# fill out jinja2 templates
	print(ad)
	for jinjable_key in ["contents", "details", "desc", "title", "footer"]: # mind the order
		if jinjable_key not in ad:
			continue
		if type(ad[jinjable_key]) is list:
			for i,jinjable in enumerate(ad[jinjable_key]):
				t = Template(jinjable)
				ad[jinjable_key][i] = t.render(ad)
		else:
			t = Template(ad[jinjable_key])
			ad[jinjable_key] = t.render(ad)
	ad["desc"] += ad["footer"]
	os.chdir(ad_file_path)
	picture_paths = []
	for path in ad["pictures"].get("paths", []):
		expanded_paths = [os.path.realpath(f) for f in glob.glob(path)]
		picture_paths += expanded_paths
	ad["pictures"]["paths"] = picture_paths
	os.chdir(prev_path)
	return ad


print(main_config)
templates = load_templates(main_config.get("template_dir")+"/templates.yml")

for ad_file in args.ad_files:
	try:
		ad = load_ad(ad_file, templates)
		print(ad)
	except:
		extype, value, tb = sys.exc_info()
		traceback.print_exc()
		pdb.post_mortem(tb)
		

	
