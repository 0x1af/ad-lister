from selenium.common import exceptions  
from selenium.webdriver.common import action_chains
from selenium.webdriver.remote import switch_to
import datetime
import time
import random



def get_ongoing_http_requests():
	return browser.execute_script('return window.openHTTPs')
		
def has_ongoing_http_requests():
	n_requests = get_ongoing_http_requests()
	if n_requests is None:
		raise Exception("monkey_patch_xmlhttprequest is missing")
	return n_requests > 0

# Source: https://stackoverflow.com/questions/9267451/how-to-check-if-http-requests-are-open-in-browser.
# This replaces the default XMLHttpRequest open with a customized variant, which keeps track of the number of ongoing connections
# 
def monkey_patch_xmlhttprequest():
	n_requests = get_ongoing_http_requests()
	if n_requests is not None:
		return # already patched, window.openHTTPs var exists
	script = '''
	(function() {
		var oldOpen = XMLHttpRequest.prototype.open;
		window.openHTTPs = 0;
		window.totalHTTPs = 0;
		XMLHttpRequest.prototype.open = function(method, url, async, user, pass) {
			window.openHTTPs++;
			window.totalHTTPs++;
			this.addEventListener('readystatechange', function() {
				if(this.readyState == 4) {
					window.openHTTPs--;
				}
			}, false);
			oldOpen.call(this, method, url, async, user, pass);
		}
	})();'''
	browser.execute_script(script)
	

# first hacky attempt: Problem: The reload indicators (reload spinner and the transparency) are not in use the first time, might be a bug
# also they might be reloaded twice sometimes for no apparent reason
class ContentReloadComplete(object):
	"""An expectation for checking that an element has a particular css class.

	locator - used to find the element
	returns the WebElement once it has the particular css class
	"""
	class State(object):
		FIND_RELOADING_CONTENT = 1
		CHECK_RELOADING_CONTENT_DONE = 2
		INTERACT_W_RELOADED_CONTENT = 3
		ALL_RELOADED = 4

	def __init__(self):
		self.reloadable_div_ids = []
		self.state = self.State.FIND_RELOADING_CONTENT
		self.n_iter = 0

	def fetch_reloadable_content_div_ids(self):
		# Whenever a div class contains "sec-msk" its children (and parent div) may be dynamically reloaded (ongoing reload is indicated by spinner and 60% transparency, but not always - see first failed attempt)
		reloadable_content_divs = browser.find_elements_by_xpath('''//div[contains(@class,"sec-msk") and @id]''')
		reloadable_content_divs += browser.find_elements_by_xpath('''//div[contains(@class,"sec-msk") and not(@id)]/parent::*/parent::div[@id]''')
		reloadable_ids = [div.get_attribute('id') for div in reloadable_content_divs]	
		return reloadable_ids
		
	def interact_with_reloaded_content(self, reloadable_div_ids):
		print("interact_with_reloaded_content")
		success = True
		for div_id in reloadable_div_ids:
			try:
				div = browser.find_element_by_id(div_id)
				print("is_displayed: %d  is_enabled: %d"%(div.is_displayed(), div.is_enabled()))
				#masks[i].click()
				#a = action_chains.ActionChains(browser)
				#div_width = div.size["width"]
				#a.move_to_element_with_offset(div, 5, div_width-5).click() # offset is given from the top left corner
				browser.execute_script("arguments[0].scrollIntoView(true);", div);
				#a.perform()
				print(div)
				print(switch_to.SwitchTo(browser).active_element)
			except exceptions.WebDriverException as e:
				print(e)
				success = False

		return success

	def is_all_reloadable_content_present(self, reloadable_div_ids):
		success = True
		for div_id in reloadable_div_ids:
			try:
				browser.find_element_by_id(div_id)
			except exceptions.WebDriverException as e:
				success = False
		return success

	def __call__(self, browser):
		print("enter %d"%(self.state))
		self.n_iter += 1
		if self.state == self.State.FIND_RELOADING_CONTENT:
			self.reloadable_div_ids = self.fetch_reloadable_content_div_ids()
			if not has_ongoing_http_requests() and self.n_iter > 1:
				self.state = self.State.ALL_RELOADED
			else:
				self.state = self.State.CHECK_RELOADING_CONTENT_DONE
		if self.state == self.State.CHECK_RELOADING_CONTENT_DONE:
			if not has_ongoing_http_requests() and self.is_all_reloadable_content_present(self.reloadable_div_ids):
				self.state = self.State.INTERACT_W_RELOADED_CONTENT
		elif self.state == self.State.INTERACT_W_RELOADED_CONTENT:
			if self.interact_with_reloaded_content(self.reloadable_div_ids):
				# sometimes content is reloaded again, once we start interacting with it - so we try one more time
				self.state = self.State.FIND_RELOADING_CONTENT
		print("exit: %d"%(self.state))
		return self.state == self.State.ALL_RELOADED

# another even worse attempt
def wait_content_reload():
	def check_staleness(reloadable_content_divs):
		stale_ids = []
		for div in reloadable_content_divs:
			div_id = div.get_attribute('id') 
			try:
				div.is_enabled()
			except exceptions.WebDriverException as e:
				stale_ids.append(div_id)
		return stale_ids
	def fetch_reloadable_conent_divs():
		reloadable_content_divs = browser.find_elements_by_xpath('''//div[contains(@class,"sec-msk") and @id]''')
		reloadable_content_divs += browser.find_elements_by_xpath('''//div[contains(@class,"sec-msk") and not(@id)]/parent::*/parent::div[@id]''')
		return reloadable_content_divs
	# Whenever a div class contains "sec-msk" its children (and parent div) may be dynamically reloaded (ongoing reload is indicated by spinner and 60% transparency, but not always - see first failed attempt)
	reloadable_content_divs = fetch_reloadable_conent_divs()
	reloadable_ids = [div.get_attribute('id') for div in reloadable_content_divs]	
	stale_ids = []
	for i in range(10):
		time.sleep(1) # ouch, any improvements greatly welcome!

		stale_ids = check_staleness(reloadable_content_divs)
		for stale_id in stale_ids:
			WebDriverWait(browser, 60).until(EC.presence_of_element_located((By.ID, stale_id)))
	

class BayLister(object):
	def __post_ad_begin(self, config, ad):
		browser.get(config["post-ad-url"])
		browser.switch_to_frame("findprod_iframe")
		search_input = browser.find_element_by_xpath('''//input[@id="w0-find-product-search-bar-search-field"]''')
		search_input.send_keys(ad["title"])
		search_button = browser.find_element_by_xpath('''//button[@id="w0-find-product-search-bar-search-button"]''')
		search_button.click()
		#browser.switch_to.parent_frame()
		#browser.switch_to_frame("findprod_iframe")
		continue_without_product_template_button = '''//*[@id="w0-find-product-2"]/div[2]/button'''
		WebDriverWait(browser, 60).until(EC.presence_of_element_located((By.XPATH, continue_without_product_template_button)))
		continue_without_product_template = browser.find_element_by_xpath(continue_without_product_template_button)
		continue_without_product_template.click()
	


	def __hide_status_div(self):
		# the status div is displayed when the form is saved, it sometiems overlays elements we wish to interact with
		browser.execute_script("document.getElementsByClassName('estatus')[0].setAttribute('style', 'display: none;')");

	def __set_title(self, title):
		title_input = browser.find_element_by_xpath('''//input[@id="editpane_title"]''')
		title_input.clear();
		title_input.send_keys(title)

	def __post_ad_fill_form(self, ad):


		#browser.implicitly_wait(5)
		select_cat_xpath = '''//*[@id="browseCatIdcat1"]'''
		select_cat_text_xpath = '''//*[@id="browseCatIdcat1Text"]'''
		select_cat = browser.find_element_by_xpath(select_cat_xpath)
		select_cat_text = browser.find_element_by_xpath(select_cat_text_xpath)

		if not select_cat_text.is_displayed() and not select_cat.is_displayed():
			print("expand first")
			# First expand the category select area, this causes category div to be reloaded, so web elements need to be fetched again
			select_recent_cat_xpath = '''//a/*[@fn="chgcat"]/parent::a'''
			select_recent_cat = browser.find_element_by_xpath(select_recent_cat_xpath)
			select_recent_cat.click()
			WebDriverWait(browser, 60).until(EC.invisibility_of_element_located( (By.XPATH, select_recent_cat_xpath) ))
			select_cat = WebDriverWait(browser, 60).until(EC.presence_of_element_located( (By.XPATH, select_cat_xpath) ))
			select_cat_text = browser.find_element_by_xpath(select_cat_text_xpath)

		if not select_cat_text.is_displayed():
			WebDriverWait(browser, 60).until(EC.visibility_of((select_cat)))
			select_cat.click() # the link is active and has to be clicked first (otherwise already selected)
			WebDriverWait(browser, 60).until(EC.visibility_of((select_cat_text)))

		cat_frame_xpath = '''//iframe[@id="browseCategoriescat1"]'''
		WebDriverWait(browser, 60).until(EC.frame_to_be_available_and_switch_to_it((By.XPATH, cat_frame_xpath)))

		# undo previous selection
		while True:
			go_back_arrows = browser.find_elements_by_xpath('''//div[@class="caty-list-spacer"]''')
			if not go_back_arrows:
				break
			for arrow in reversed(go_back_arrows):
				if not arrow.is_displayed():
					continue
				arrow.click()
				WebDriverWait(browser, 60).until(EC.staleness_of(arrow))
				break

			
		for i, category in enumerate(ad["ad_category"]):
			ul_xpath = "//ul[@id=\"caty_%u\"]"
			cat_xpath = ul_xpath%(i+1) + "/li[contains(text(), \"%s\")]"%(category)
			print(cat_xpath)
			WebDriverWait(browser, 60).until(EC.presence_of_element_located((By.XPATH, cat_xpath)))
			cat = WebDriverWait(browser, 60).until(EC.element_to_be_clickable((By.XPATH, cat_xpath)))
			cat.click()

		browser.switch_to.parent_frame()

	def __set_item_condition(self, ad):
		item_condition = Select(browser.find_element_by_xpath('''//select[@id="itemCondition"]'''))
		item_condition.select_by_visible_text("Gebraucht")

	def __set_item_properties(self, ad):
		if not "props" in ad:
			return
		try:
			show_more = browser.find_element_by_xpath('''//span[@class="eib-lnkw"]/a[@class="eib-more"]''')
			if show_more.is_displayed():
				show_more.click()
		except Exception as e:
			print(e)

		for prop, value in ad["props"].items():
			try:
				translated_prop = ad["translator"][prop]
				item_specific_xpath = '''//*[@id="editpane_Listing.Item.ItemSpecific['''+translated_prop+''']"]//'''
				print(item_specific_xpath)
				button = browser.find_element_by_xpath(item_specific_xpath+"button")
				button.click()
			
				WebDriverWait(browser, 60).until(EC.presence_of_element_located((By.XPATH, item_specific_xpath+"ul"))) # drop down list
				try:
					selection = browser.find_element_by_xpath(item_specific_xpath+"ul//*[contains(translate(text(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'),\"%s\")]"%(value.lower()) )
					selection.click()
				except Exception as e:
					print("Could not find %s (%s) for %s in values list. Using own."%(translated_prop, prop, value))
					input_elem = browser.find_element_by_xpath(item_specific_xpath+"input")
					input_elem.clear()
					if type(value) is list:
						value = ', '.join(value)	
					input_elem.send_keys(value)
			except Exception as e:
				print("Error: setting prop '%s = %s' failed: %s"%(prop, value, e))

	def __upload_pictures(self, picture_paths):
		'''//*[@id="tg-thumbs"]//a[@class="del" and @role="button" and @style="display: block;"]'''

		WebDriverWait(browser, 60).until(EC.frame_to_be_available_and_switch_to_it((By.XPATH, '''//iframe[@id="uploader_iframe"]''')))
		for delete_all_button in browser.find_elements_by_xpath('''//a[@role="button" and @class="deleteAll"]'''):
			# there exist multiple - one of them is visible
			if delete_all_button.is_displayed():
				delete_all_button.click()
				confirm_button = browser.find_element_by_xpath('''//div[starts-with(@id,"puDlg")]//div[@class="btns"]/a[@class="b1 btn btn-m btn-ter"]''') # HAHA b1 btn btn-m btn-ter
				confirm_button.click()
				continue
		thumbnail_count_match = '''//*[@id="tg-thumbs" and count(.//div[@class="thumb" and @style="display: block;"])=%d]'''
		WebDriverWait(browser, 60).until(EC.presence_of_element_located((By.XPATH, thumbnail_count_match%(0) )))
	
		for i, pic_path in enumerate(picture_paths):
			upload_pic_input = browser.find_element_by_xpath('''//input[@class="upl-fileInp"]''')
			upload_pic_input.send_keys(pic_path)
			# wait for the uplaod to be completed
			WebDriverWait(browser, 60).until(EC.presence_of_element_located((By.XPATH, thumbnail_count_match%(i+1) )))
		browser.switch_to.parent_frame()
	
	def __enter_ad_description(self, ad):
		html_mode = browser.find_element_by_xpath('''//div[@id="descDiv"]//a[text() = "HTML"]''')
		html_mode.click()
	
		desc_iframe = browser.find_element_by_xpath('''//iframe[contains(@id, "txtEdit_ht") and @title="HTML"]''') # the text is written to an iframe and then transferred to a hidden input 'description'
		desc_iframe.send_keys(Keys.CONTROL + "a") # any existing text is cleared by pressing "select all" and then sending the new description
		desc_iframe.send_keys(ad["desc"])
		#//*[@id="Listing.Item.ItemSpecific[Speichergröße]"]
		#ean_input = browser.find_element_by_xpath('''//input[@id="ean"]//*[@id="Listing.Item.ItemSpecific[Marke]"]''')
		#ean_input.send_keys("None")

	def __set_ad_fixed_price(self, ad):
		ad_format = Select(browser.find_element_by_xpath('''//div[@id= "editpane_forDur"]//select[@id="format"]'''))
		ad_format.select_by_value("FixedPrice")

		try:
			start_price_label = browser.find_element_by_xpath('''//label[@for="startPrice"]''')
			WebDriverWait(browser, 5).until(EC.staleness_of(start_price_label))  # the start price label should no longer exist, then relevant part of the form has been updated
		except Exception as e:
			print(e)

		price_input = browser.find_element_by_xpath('''//div[@id= "editpane_forDur"]//input[@id="binPrice"]''')
		price_input.clear()
		price_input.send_keys(ad["price"])
	
		quantity_input = browser.find_element_by_xpath('''//div[@id= "editpane_forDur"]//input[@id="quantity"]''')
		quantity_input.clear()
		quantity_input.send_keys(str(ad["quantity"]))
	
		bestoffer_checkbox = browser.find_element_by_xpath('''//div[@id= "editpane_forDur"]//input[@id="bestOffer" and @type="checkbox"]''')
		if bestoffer_checkbox.is_selected() != ad["allow_best_offer"]:
			bestoffer_checkbox.click()
	
		returns_checkbox = browser.find_element_by_xpath('''//input[@id="returnsAccepted" and @type="checkbox"]''')
		international_returns_checkbox = browser.find_element_by_xpath('''//input[@id="internationalReturnsAccepted" and @type="checkbox"]''')
	
		if returns_checkbox.is_selected() != ad["allow_return"]:
			returns_checkbox.click()

		if international_returns_checkbox.is_selected() != ad["allow_return"]:
			international_returns_checkbox.click()

	def __remove_all_national_shipping(self):
		remove_links = browser.find_elements_by_xpath('''//div[@id="editpane_domShip"]//a[contains(text(), "Versandart entfernen")]''')
		for remove in remove_links:
			if not remove.is_displayed():
				continue
			remove.click()


	def __set_ad_shipping(self, ad):
		print(datetime.datetime.now().time())	
		enable_shipping = WebDriverWait(browser, 60).until(EC.element_to_be_clickable((By.XPATH, '''//select[@id="domesticShipping"]''' ))) 
		print(datetime.datetime.now().time())

		#enable_shipping = Select(browser.find_element_by_xpath('''//select[@id="domesticShipping"]'''))
		Select(enable_shipping).select_by_value("FLAT_RATE")
		ship_select_xpath_scheme = '''//div[@id="editpane_domShip"]//select[@id="shipService%d"]'''
		WebDriverWait(browser, 60).until(EC.presence_of_element_located((By.XPATH, ship_select_xpath_scheme%(1) ))) 

		self.__remove_all_national_shipping()
		for i, shipping in enumerate(ad["shipping"]["national"]):
			if i > 0:
				add_shipping = browser.find_element_by_xpath('''//div[@id="editpane_domShip"]//a[@id="service_add"]''')
				add_shipping.click()
			ship_select_xpath = ship_select_xpath_scheme%(i+1)
			ship_select = Select(browser.find_element_by_xpath(ship_select_xpath))

			for option in ship_select.options:
				if shipping["service"] in option.text:
					ship_select.select_by_value(option.get_attribute('value'))
					break
			ship_fee = browser.find_element_by_xpath('''//div[@id="editpane_domShip"]//input[@id="shipFee%d"]'''%(i+1) )
			ship_fee.clear()
			ship_fee.send_keys(shipping["price"])

		delay_days = ad["shipping"]["delay"]
		ship_delay = Select(browser.find_element_by_xpath('''//div[@id="editpane_domShip"]//select[@id="shipsWithinDays"]'''))
	
		for option in ship_delay.options: # assuming sorted values
			if delay_days in option.text:
				ship_delay.select_by_visible_text(option.text)
				break

		intl_ship_div_xpath = '''//div[@id="editpane_intlShip"]'''
		intl_ship_div = browser.find_element_by_xpath(intl_ship_div_xpath)
		intl_ship_select_xpath_scheme = intl_ship_div_xpath + '''//select[@id="intlShipOptions%d"]'''

		intl_shipping = Select(intl_ship_div.find_element_by_xpath('''.//select[@id="intlShipping"]'''))

		if ad["shipping"]["international"]:
			intl_shipping.select_by_value("FLAT_RATE")

			WebDriverWait(browser, 60).until(EC.element_to_be_clickable((By.XPATH, intl_ship_select_xpath_scheme%(1) ))) 
			self.__select_international_shipping(ad, intl_ship_div_xpath)
		else:
			active_selection = intl_shipping.first_selected_option.get_attribute("value")
			if active_selection != "NOT_SPECIFIED":
				intl_shipping.select_by_value("NOT_SPECIFIED")
				WebDriverWait(browser, 60).until(EC.invisibility_of_element_located((By.XPATH, intl_ship_select_xpath_scheme%(1) ))) 

	def __select_international_shipping(self, ad, intl_ship_div_xpath):
		def select_country_checkboxes(checkboxes, target_destinations):
			for checkbox in checkboxes:
				country = checkbox.get_attribute("value")
				is_selected = checkbox.is_selected()
				if country in target_destinations:
					if not is_selected:
						checkbox.click()
				elif is_selected:
					checkbox.click()

		intl_ship_div = browser.find_element_by_xpath(intl_ship_div_xpath)
		remove_links = intl_ship_div.find_elements_by_xpath('''.//a[@class="intl-remove"]''')
		for remove_link in remove_links:
			if remove_link.is_displayed():
				remove_link.click()

		intl_ship_select_xpath_scheme = intl_ship_div_xpath + '''//select[@id="intlShipOptions%d"]'''

		for i, shipping in enumerate(ad["shipping"]["international"]):
			intl_ship_div = browser.find_element_by_xpath(intl_ship_div_xpath)
			if i > 0:
				add_shipping = intl_ship_div.find_element_by_xpath('''.//a[@class="intl-add"]''')
				add_shipping.click()
			intl_ship_select = Select(intl_ship_div.find_element_by_xpath(intl_ship_select_xpath_scheme%(i+1)))
			intl_ship_select.select_by_value("2") # specify locations
			country_checkboxes = intl_ship_div.find_elements_by_xpath('''.//input[@type="checkbox" and starts-with(@id, "intlLocations%d_")]'''%(i+1))
			select_country_checkboxes(country_checkboxes, shipping["destinations"])
			

			intl_ship_select_service = Select(intl_ship_div.find_element_by_xpath('''.//select[@id="intlShipService%d"]'''%(i+1) ))

			for option in intl_ship_select_service.options:
				if shipping["service"] in option.text:
					intl_ship_select_service.select_by_value(option.get_attribute('value'))
					break	
			ship_fee = intl_ship_div.find_element_by_xpath('''.//input[@id="intlShipFee%d"]'''%(i+1) )
			ship_fee.clear()
			ship_fee.send_keys(shipping["price"])

		intl_shipping_demand = Select(intl_ship_div.find_element_by_xpath('''//select[@id="shipOptions"]'''))
		intl_shipping_demand.select_by_value("1") # select specify locations
		country_checkboxes = intl_ship_div.find_elements_by_xpath('''.//input[contains(@id, "globalLocations_")]''')
		select_country_checkboxes(country_checkboxes, ad["shipping"]["international_ondemand"]["destinations"])

	def __check_fee(self, ad):
		show_fees = browser.find_element_by_xpath('//a[@id="rev_fees"]')
		show_fees.click()
		popup_dialog_fee_amount = WebDriverWait(browser, 60).until(EC.presence_of_element_located((By.XPATH, '//span[@class="refe-totfees"]' )))
		if ad["is_zero_fee"]:
			for digit in filter(lambda i: i.isdigit(), popup_dialog_fee_amount.text):
				if digit != "0":
					raise Exception("Not a zero fee ad.")
		dialog_close_button = WebDriverWait(browser, 60).until(EC.visibility_of_element_located( (By.XPATH, '//a[@id="reviewFeeLyr_cbtn"]') ))
		dialog_close_button.click()
		WebDriverWait(browser, 60).until(EC.invisibility_of_element_located((By.XPATH, '//span[@class="refe-totfees"]' )))

	def __confirm_ad(self):
		confirm_button = browser.find_element_by_xpath('//div[@id="actionbar"]/input[@class="pbtn"  and @fn="pub"]')
		confirm_button.click()

	def list_new(self, ad, config):
		#ad = load_ad(args.ad_files[0], templates)
		post_ad_begin(config, ad)
		self.__hide_status_div()
		self.__post_ad_fill_form(ad)
		WebDriverWait(browser, 60).until(ContentReloadComplete())
		self.__set_item_properties(ad)
		self.__upload_pictures(ad)
		print("done")
		self.__enter_ad_description(ad)

		self.__set_ad_fixed_price(ad)


		WebDriverWait(browser, 60).until(ContentReloadComplete())
		#WebDriverWait(browser, 10).until(EC.staleness_of(to_be_replaced))
		print("stale")
		#WebDriverWait(browser, 10).until(EC.presence_of_element_located((By.XPATH, '''//div[@id="editpane_domShip"]''' ))) 
		#print("great!")
		#WebDriverWait(browser, 60).until(element_has_full_opacity())
		self.__check_fee(ad)

	def modify(self, existing_id, ad, config):
		browser.get(config["modify_ad_url_scheme"].format(existing_id=existing_id) )
		monkey_patch_xmlhttprequest()
		self.__hide_status_div()
		self.__post_ad_fill_form(ad)
		WebDriverWait(browser, 60).until(ContentReloadComplete())
		self.__set_item_condition(ad)
		self.__set_title(ad["title"])
		WebDriverWait(browser, 60).until(ContentReloadComplete())
		self.__set_item_properties(ad)
		self.__upload_pictures(ad["pictures"]["paths"])
		self.__enter_ad_description(ad)
		self.__set_ad_shipping(ad) # 
		self.__set_ad_fixed_price(ad)
		WebDriverWait(browser, 60).until(ContentReloadComplete())
		self.__check_fee(ad)

	def __build_clone_title(self, title_conf):
		title = title_conf["head"]
		max_pieces = len(title_conf["pieces"])
		n_pieces = random.randint(max_pieces-1, max_pieces)
		chosen_pieces = sorted(random.sample(range(max_pieces),k=n_pieces))
		for chosen_piece in chosen_pieces:
			piece_options = title_conf["pieces"][chosen_piece]
			title += " " + random.choice(piece_options)
		return title

	def clone(self, existing_id, ad, config, title=None):
		if title is None:
			self.__build_clone_title(ad["clone_title"])
		browser.get(config["clone_ad_url_scheme"].format(existing_id=existing_id) )
		monkey_patch_xmlhttprequest()
		title = self.__build_clone_title(ad["clone_title"])
		self.__set_title(title)

		picture = random.choice(ad["pictures"]["paths"])
		pic_matching = ad.get("picture_matching", {})
		for keyword, path_pattern in pic_matching.items():
			if not keyword in title:
				continue
			for path in ad["pictures"]["paths"]:
				if path_pattern in path:
					picture = path
		
		self.__upload_pictures([picture])
		WebDriverWait(browser, 60).until(ContentReloadComplete())
		self.__check_fee(ad)		
		self.__confirm_ad()
	
	def __generate_titles(self, n, title_conf, exclude_titles=set()):
		titles = set()
		stuck = 0
		while len(titles) < n and stuck < 20:		
			len_before = len(titles)
			title = self.__build_clone_title(title_conf)
			if title not in exclude_titles:
				titles.add(title)
			if len_before == len(titles):
				stuck +=1
		print(titles)
		if stuck >= 20:
			raise Exception("Not enough variations")
		
		return titles

	def clone_multiple(self, n, existing_id, ad, config, exclude_titles=set()):
		
		titles = self.__generate_titles(n, ad["clone_title"], exclude_titles)
		for title in titles:
			self.clone(existing_id, ad, config, title)
			
			time.sleep(10)

	def price_n_shipping(self):
		self.__set_ad_fixed_price(ad)
		WebDriverWait(browser, 60).until(ContentReloadComplete())
		self.__set_ad_shipping(ad)
		self.__check_fee(ad)

