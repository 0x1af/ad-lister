import pdb
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC  
from selenium.webdriver.common.by import By
import time 
import os
from configparser import ConfigParser

import argparse

from loadconfig import Config
# TODO: switch to https://pypi.org/project/HiYaPyCo/

import os
import argparse
import yaml
from jinja2 import Template, Undefined
from collections import OrderedDict
import pdb


def load_main_config(config_file, user_file):
	config = {}
	with open(config_file) as fh: 
		config = Config(fh.read())
	with open(user_file) as fh: 
		config.update(Config(fh.read()))
	return config


def parse_args():
	parser = argparse.ArgumentParser(description="Ad poster")
	parser.add_argument('-u', '--user-file', default='user.yml', help='file which contains user name and pass')
	parser.add_argument('-c', '--config-file', default='config.yml', help='web config file')
	parser.add_argument('ad_files', nargs="*", help='ad files')
	return parser.parse_args()


def wait_for_page(title):
	if not title in browser.title:
		wait = WebDriverWait(browser, 1000)
		wait.until(EC.title_contains(title))
		print("Possible captcha. Waiting for login page")

def check_skip_update_contact_info():
	if not "Kontaktinfo aktualisieren" in browser.title:
		return
	remind_later = browser.find_element_by_xpath('''//a[@id="rmdLtr"]''')
	remind_later.click()

def open_login():
	browser.get("https://ebay.de")
	
	login_link = browser.find_element_by_xpath('''//*[@id="gh-ug"]/a[contains(text(),"Einloggen")]''')
	
	time.sleep(12)

	login_link.click()
	

def login(config):
	
	#browser.get(config["post-ad-url"])
	wait_for_page("Einloggen")
	username_elem = browser.find_element_by_xpath('''//input[@id="userid"]''')
	pwd_elem = browser.find_element_by_xpath('''//input[@id="pass" and @type="password"]''')
	username_elem.send_keys(config.username)
	pwd_elem.send_keys(config.pwd)

	login_submit = browser.find_element_by_xpath('''//button[@id="sgnBt" and @type="submit"]''')
	login_submit.click()

	check_skip_update_contact_info()
		


args = parse_args()
print(args)
main_config = load_main_config(args.config_file, args.user_file)
print(main_config)


browser = webdriver.Chrome("chromedriver")
open_login()
login(main_config)




